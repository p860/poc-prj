import { Component, OnInit } from '@angular/core';
import { products } from './products';
import { SelectableSettings } from '@progress/kendo-angular-grid';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EditService } from './edit.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public gridData: any[] = products;
  public checkboxOnly = false;
  public mode = 'multiple';
  public drag = false;
  public selectableSettings: SelectableSettings | any;
  public selectedValues: any[] = [];
  public clipboard: any;
  public columns = ['ProductName'];
  isPasted = false;
  pastedMap: { itemKey: number; columnKey: number }[] = [];
  constructor(
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private editService: EditService
  ) {
    this.setSelectableSettings();
    console.log(this.gridData);
  }

  ngOnInit() {
    this.gridData = this.editService.data;
  }

  public onSelect({ dataItem, item }): void {
    if (item === 'Copy Value') {
      this.clipboard = [...this.selectedValues];
      console.log('CLIPBOARD', this.clipboard);
    } else {
      this.onPaste();
    }
  }

  onPaste() {
    const copyValues: any[] = [];
    this.clipboard.forEach((item: any) =>
      copyValues.push(this.gridData[item.itemKey - 1][columns[item.columnKey]])
    );
    console.log('COPY VALUES', copyValues);
    console.log('SLECTED VALUES', this.selectedValues);
    this.selectedValues.forEach((item) => {
      if (
        !this.pastedMap.find(
          (p) => p.itemKey === item.itemKey && p.columnKey === item.columnKey
        )
      ) {
        this.pastedMap.push(item);
      }
    });
    this.isPasted = true;
    this.selectedValues.forEach((val) => {
      this.gridData.find((item, i) => {
        if (item.ProductID === val.itemKey) {
          item[columns[val.columnKey]] = copyValues[0];
          this.isPasted = true;
        }
      });
    });
  }

  selectionChange(event: any) {
    console.log(event);
    this.selectedValues = event.selectedCells;
    const copyValues: any[] = [];
    this.selectedValues.forEach((item) =>
      copyValues.push(this.gridData[item.itemKey - 1][columns[item.columnKey]])
    );
    if (this.selectedValues.length > 1) {
      this.selectedValues.forEach((item) => {
        if (
          !this.pastedMap.find(
            (p) => p.itemKey === item.itemKey && p.columnKey === item.columnKey
          )
        ) {
          this.pastedMap.push(item);
        }
      });

      this.selectedValues.forEach((val) => {
        this.gridData.find((item) => {
          if (item.ProductID === val.itemKey) {
            item[columns[val.columnKey]] = copyValues[0];
            this.isPasted = true;
          }
        });
      });
    }
  }

  public setSelectableSettings(): void {
    if (this.mode === 'single') {
      this.drag = false;
    }

    this.selectableSettings = {
      cell: true,
      mode: this.mode,
      drag: this.drag,
    };
  }

  public colorCode(rowId: number, columnId: number): SafeStyle {
    let result = 'transparent';
    if (
      this.pastedMap.find(
        (item) => item.itemKey == rowId && item.columnKey == columnId
      )
    ) {
      result = '#fcebdc';
    }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public cellClickHandler({
    sender,
    rowIndex,
    columnIndex,
    dataItem,
    isEdited,
  }) {
    if (!isEdited && !this.drag) {
      sender.editCell(rowIndex, columnIndex, this.createFormGroup(dataItem));
    }
  }

  public cellCloseHandler(args: any) {
    const { formGroup, dataItem } = args;

    if (!formGroup.valid) {
      // prevent closing the edited cell if there are invalid values.
      args.preventDefault();
    } else if (formGroup.dirty) {
      console.log('DATA ITEM', dataItem);
      this.editService.assignValues(dataItem, formGroup.value);
      const index = this.editService.data.indexOf(dataItem);
      this.editService.data.splice(index, 1, dataItem);
      this.gridData = this.editService.data;
      console.log('GRID AFTER', this.gridData);
      // this.editService.update(dataItem);
    }
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this.formBuilder.group({
      ProductName: [dataItem.ProductName, Validators.required],
      UnitsonOrder: dataItem.UnitsonOrder,
      UnitsInStock: [
        dataItem.UnitsInStock,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[0-9]{1,3}'),
        ]),
      ],
      Discontinued: dataItem.Discontinued,
    });
  }
}
export const columns = [
  'ProductName',
  'UnitsInStock',
  'Discounted',
  'UnitsOnOrder',
  'ReorderLevel',
];
