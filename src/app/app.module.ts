import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridContextMenuComponent } from './grid-context-menu/grid-context-menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DialogModule, DialogsModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { EditService } from './edit.service';




@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    GridModule,
    PopupModule,
    DialogsModule,
    ReactiveFormsModule,
    DropDownsModule,
    DialogModule,
    HttpClientModule,
    HttpClientJsonpModule

  ],
  declarations: [
    AppComponent,
    GridContextMenuComponent
  ],
  providers: [EditService],
  bootstrap: [AppComponent]
})
export class AppModule { }
