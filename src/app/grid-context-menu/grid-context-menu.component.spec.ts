import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridContextMenuComponent } from './grid-context-menu.component';

describe('GridContextMenuComponent', () => {
  let component: GridContextMenuComponent;
  let fixture: ComponentFixture<GridContextMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridContextMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
