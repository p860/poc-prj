export class Product {
    public ProductID?: number | any;
    public ProductName = "";
    public Discontinued = false;
    public UnitsInStock: number | any;
    public UnitsOnOrder: number | any;
    public UnitPrice? = 0;
  }